# Delegates
The word 'delegates' gets thrown around a lot when talking about c# at work.  It's time I figured out what they were and why they are so powerful.
From the [C# Programming Guide](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/delegates/index):

    A delegate is a type that represents references to methods with a particular parameter list and return type.

### stream-of-consciousness writing
so a delegate is a type. It is a substitution for the as  a nethod oof some spoirt sort. It ihas  stub that includes a scope, 'delegate' keyword, return type, and name and then arguments. it is mentione cdthat its similar to a aa d ; a what , brain derailed, as mentioned that it is similar to c++ funtion pointers. so I don't know what that means. I have to find another anlolgy for that. I see it looks very similar to a method stuf in an interface. I think I'll need to learn more before I get more out of the SOC exercise.

### 11-30-2020
I found something that put a big piece of the puzzle in place.\
From [here](https://dzone.com/articles/c-basics-delegates):
> The signature of a delegate is very important, because a delegate can only refer to functions matching the same signature.
 
So **THAT** is where the subsitutution behavior that everyone talks about comes from. If I have a delegate that returns a string and takes in two ints, ANY function that returns a string with two int parameters can be used by this delegate!

