
using System;

namespace csharp.delegates {

    /// <summary>
    /// First Lesson
    /// </summary>
    class SuperSimpleDelegate
    {
        //TODO: find out why it has to return type 'void'
        public delegate void greetingMethod(string name);
        /* this is commented out because a 'main' function already exists in Program.cs
        public static void Main() {
            ReturnGreeting("Ben");

            greetingMethod myDelGreeting = new greetingMethod(ReturnGreeting);
            myDelGreeting.Invoke("Jooky");

            myDelGreeting = new greetingMethod(VerticalGreeting);
            myDelGreeting.Invoke("Zapp Rowsdower");
        }
        */

        public static void ReturnGreeting(string name) {
            string response = "hello, " + name + "!";
            System.Console.WriteLine(response);
        }

        public static void VerticalGreeting(string name)
        {
            string response = "hello, " + name + "!";
            foreach(var a in response)
            {
                System.Console.WriteLine(a);
            }
        }

        public static void GiveNumericGreeting(string name)
        {
            //use the name as a seed for a random integer
            Random randomGreeting = new Random(name.GetHashCode());
            int nameAsInt = randomGreeting.Next(0, 100);
            string response = "hello, " + nameAsInt + "!";
            System.Console.WriteLine(response);
        }
    }
}

