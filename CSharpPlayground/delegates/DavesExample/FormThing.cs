
using DavesExample;

class FormThing
{

    private LightSwitch light;
    private Toaster toaster;
    private Lamp lamp;

    public FormThing() {
        light = new LightSwitch();
        toaster = new Toaster();
        lamp = new Lamp();
    }

    public void Initialize()
    {
        light.FlipSwitch();

        //on toaster click
        light.Power = toaster.ChangeColor;

        //on lamp click
        light.Power = lamp.ChangeColor;
    }
}