namespace DavesExample
{
    class Lamp
    {
        string box;

        public void Initialize(string inputBox)
        {
            box = inputBox;

        }

        public void ChangeColor(bool pOn)
        {
            if(pOn)
            {
                box = "blue";

            }
            else 
            {
                box = "black";
            }
        }
    }
}