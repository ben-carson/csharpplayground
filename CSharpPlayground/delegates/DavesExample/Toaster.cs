namespace DavesExample
{
    class Toaster
    {
        string box;

        public void Initialize(string inputBox)
        {
            box = inputBox;

        }

        public void ChangeColor(bool pOn)
        {
            if(pOn)
            {
                box = "red";

            }
            else 
            {
                box = "black";
            }
        }
    }
}