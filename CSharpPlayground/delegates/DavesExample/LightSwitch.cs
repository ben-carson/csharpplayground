namespace DavesExample 
{
    class LightSwitch
    {
        public delegate void GenericDelegate(bool pOn);

        public GenericDelegate Power;
        bool on;

        public bool On {
            get { return on; }
        }

        public void FlipSwitch()
        {
            on = !on;
            if(Power != null)
            {
                Power(on);
            }
        }
    }
}