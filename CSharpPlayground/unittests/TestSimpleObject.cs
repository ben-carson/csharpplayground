using NUnit.Framework;
using NSubstitute;

namespace CSharpPlayground
{
    [TestFixture]
    public class TestISimpleObject
    {
        [Test]
        public void TestMockOfRandom()
        {
            var rand = NSubstitute.Substitute.For<System.Random>();
            rand.Next(Arg.Any<int>(), Arg.Any<int>()).Returns(270);
            Assert.That(270 == rand.Next(0,361));
        }
    }
}