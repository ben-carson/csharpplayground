﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpPlayground.JSON
{
    class JSONwork
    {
        //static void Main(string[] args)
        public void DoJSONWork()
        {

            AutoBuildObject myObj = new AutoBuildObject();
            myObj.buildName = "Test_Name";
            myObj.iconFilePath = "some file path, no biggie!";
            myObj.version = "0.0.1";
            myObj.appSettingsFilePath = "application/Settings/path";
            List<string> buildScenes = new List<string>() { "scene1", "file/path/scene2", "file/path2/scene3" };
            myObj.scenesToBuild = buildScenes;
            string json = JsonConvert.SerializeObject(myObj);
            Console.WriteLine(json);
            try
            {
                File.WriteAllText("./myObj.json", json);
            }
            catch (DirectoryNotFoundException dnfe)
            {
                Console.WriteLine(dnfe.Message);
            }

        }
    }

    class AutoBuildObject
    {
        public string buildName;
        public string iconFilePath;
        public string version;
        public string appSettingsFilePath;
        public List<string> scenesToBuild;
    }
}
