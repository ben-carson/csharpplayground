# Lambdas
Here is a space where I'll stash any experiments I create around my learning of lambdas

Per the Microsoft reference:

    A lambda expression is a block of code (an expression or a statement block) that is treated as an object. It can be passed as an argument to methods, and it can also be returned by method calls.
