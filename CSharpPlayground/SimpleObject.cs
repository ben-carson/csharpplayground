using System;

namespace CSharpPlayground
{
    class SimpleObject : ISimpleObject
    {
        //I do NOT understand the purpose of auto-generated property methods
        //They appear to be meaningless if they have to be declared public
        private string greeting;
        public string Greeting {
            get { return greeting; }
            set { greeting = value; }
        }

        private int numberValue;
        public int NumberValue {
            get { return numberValue; }
            set { numberValue = value; }
        }

        //default constructor
        public SimpleObject() {
            Greeting = "default";
            NumberValue = 0;
        }

        //parameterized contructor
        public SimpleObject(string newGreet, int newInt) {
            Greeting = newGreet;
            NumberValue = newInt;
        }

        public int getRandomValue() {
            Random rand = new Random();
            return rand.Next();
        }

        //print the value with the default greeting and number value
        public void printGreeting() {
            printGreeting("empty", -1);
        }

        //print the greeting with a random value
        public void printGreeting(string newGreeting) {
            Greeting = newGreeting;
            printGreeting(Greeting, getRandomValue());
        }

        //print the provided inputs
        public void printGreeting(string newGreeting, int newValue) {
            Greeting = newGreeting;
            NumberValue = newValue;
            Console.WriteLine(Greeting + ", " + NumberValue);
        }
    }
}
