# CSharpPlayground

Various experiments with C#\
This particular project was created for VS Code on the command line:

     dotnet new console -o DotNetJunk

### dotnet packages added
1. dotnet add package NSubstitute --version 4.2.0
1. dotnet add package NSubstitute.Analyzers.CSharp --version 1.0.10
1. dotnet add package NUnit --version 3.12.0
#### copied from [this person's](https://blog.thecodewhisperer.com/permalink/from-zero-to-nunit-with-visual-studio-code) blog
1. dotnet add package Microsoft.NET.Test.Sdk
1. dotnet add package Nunit3TestAdapter

### Run Tests

     dotnet tests

I was getting a "Program has more than one entry point defined." error at first. Turns out that is because of the "Main" method in Program.cs. NUnit injects it's own "Main" method which causes two to exist, thus the run-time error. After commenting the Program.Main method out, the tests ran fine.

### Other miscellaneous tidbits
* [Hanselman](https://www.hanselman.com/blog/HowToAccessNuGetWhenNuGetorgIsDownOrYoureOnAPlane.aspx) on how to maintain an emergency offline Nuget instance, my case for our network 
* We are also using Nexus, so I should just upload all my .nupkg files there
  * TODO: create a script for bulk upload...
*