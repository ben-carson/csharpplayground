﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpPlayground.basics
{
    /// <summary>
    /// Not sure of the exact different between readonly and const, so I made a file
    /// to better understand
    /// in a nutshell: const for compiletime, readonly for runtime
    /// </summary>
    class ROvsConst
    {
        private const string privateConstant = "this value must be provided at compile time.";
        private readonly string privateReadonly;

        public ROvsConst()
        {
            privateReadonly = "default constructor";
        }

        public ROvsConst(string newVal)
        {
            privateReadonly = newVal + " Constructor";
        }

        public void PrintValues()
        {
            Console.WriteLine($"\nPrivate const: \"{privateConstant}\"");
            Console.WriteLine($"Private readonly: \"{privateReadonly}\"\n");
        }

    }
}
