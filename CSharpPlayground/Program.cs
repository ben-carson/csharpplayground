﻿using System;
using static csharp.delegates.SuperSimpleDelegate;

namespace CSharpPlayground
{
    class Program
    {
        static void Main(string[] args)
        {

            ISimpleObject simp = new SimpleObject();
            //using these stupid C# properties instead of accessor methods. *eyeroll*
            simp.Greeting = "Hello";
            simp.NumberValue = 42;

            Console.WriteLine("The current time is " + DateTime.Now);
            simp.printGreeting();


			#region Basics
			CSharpPlayground.basics.StringBasics.SimpleStringCheck("I wonder if they have beer on the sun.");
            CSharpPlayground.basics.ROvsConst playObject = new basics.ROvsConst();
            playObject.PrintValues();
            //set a readonly value via constructor
            playObject = new basics.ROvsConst("readonly set via");
            playObject.PrintValues();
			#endregion

			#region JSON
			CSharpPlayground.JSON.JSONwork jWork = new JSON.JSONwork();
            jWork.DoJSONWork();
			#endregion

			#region Delegates
			greetingMethod myDelGreeting = new greetingMethod(ReturnGreeting);
            myDelGreeting.Invoke("Zapp Rowsdower");

            //switch delegate to new function
            myDelGreeting = VerticalGreeting;

            //delegate can also be MULTICAST, meaning more than one function fires when called
            myDelGreeting += GiveNumericGreeting;

            //also delegates can be called with normal function call syntax (aka without .Invoke())
            myDelGreeting("Jooky");
			#endregion

			//hack so that the Immediate console window will stay open once all commands have executed
			Console.WriteLine("Press any ENTER to close this window.");
            Console.ReadLine();

        }
    }
}
