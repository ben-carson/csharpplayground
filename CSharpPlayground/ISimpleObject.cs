using System;

namespace CSharpPlayground
{
    public interface ISimpleObject {

        string Greeting {get; set;}
        int NumberValue {get; set;}
        int getRandomValue();
        void printGreeting();
        void printGreeting(string greeting);
        void printGreeting(string greeting,int number);

    }
}